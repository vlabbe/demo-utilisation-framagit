# 1. Premières étapes (à faire tous ensemble)

 * Création d'un compte par élève
 * Rejoindre le groupe @projets-bac-2018-isn-montesquieu-72.
 * Création d'un projet par binôme (droit d'accès: privé).
 * Dans le projet, `Settings`, `General`, `Permissions`.
Activer "Repository", désactiver tout le reste
(Issues, Merge Requests, Pipelines, Wiki, Snippets) et sauvegarder.

# 2. Le dépot (*Repository*): qu'est-ce que c'est ?

Le dépôt, c'est le système de stockage de fichiers.
 * Il contient un système de suivi de versions. On peut suivre l'évolution du
 projet dans le temps.
 * Chaque ajout ou modification de fichier s'appelle une validation (en anglais: "commit").
 * Pour télécharger un fichier (.py par exemple) qui est sur le dépôt: avec le 
navigateur, sélectionner le fichier dans le dépôt puis faire un clic droit sur
le bouton `open raw` et choisir `enregistrer la cible du lien sous...`

# 3. Comment faire un "commit" ?

**Chaque séance** doit donner lieu à **au moins 1 commit** par groupe.

**Dans le menu `Files`, on peut** avec le bonton `+`: 
- Créer un nouveau fichier avec `New file`
- Téléverser un nouveau fichier avec `Upload file`
- Créer un nouveau dossier avec `New directory`


**Dans le menu `Files`, après avoir cliqué sur un fichier particulier, on peut l'éditer (`EDIT`), le remplacer (`REPLACE`), ou le supprimer (`DELETE`)**


