# 1. Qu'est-ce qu'une forge logicielle ?

Une [forge](https://fr.wikipedia.org/wiki/Forge_(informatique)) est un système de gestion de développement collaboratif de logiciel.

## 1.1 Exemples de projets hébergés sur une forge logicielle

La forge logicielle la plus populaire est [GitHub](https://github.com/).
Elle est utilisée par de nombreuses entreprises et de nombreux développeurs.

Quelques exemples de projets hébergés sur GitHub:
* Les [fichiers source](https://github.com/python/cpython) du langage Python 
* Un projet de détection d'objets dans des images, par le centre de recherche de Facebook sur l'IA : [Detectron](https://github.com/facebookresearch/Detectron).
* Le langage de programmation par blocs [Blockly](https://github.com/google/blockly) de Google
* Le [systeme d'exploitation](https://github.com/numworks/epsilon) de la calculatrice française [NumWorks](https://www.numworks.com/fr/)
* Une application web pour programmer en python: [Jupyter](https://github.com/jupyter/notebook). Et deux exemples de modules "tortue python": [exemple 1](https://github.com/takluyver/mobilechelonian), [exemple 2](https://github.com/gkvoelkl/ipython-turtle-widget)
* Le jeu [2048](https://github.com/gabrielecirulli/2048) (jouer [ici](http://gabrielecirulli.github.io/2048/)).

## 1.2 Framagit : une forge libre

Framagit est un forge logicielle hébergée par l'association [framasoft](https://fr.wikipedia.org/wiki/Framasoft). Ce service utilise
le logiciel libre [GitLab CE](https://fr.wikipedia.org/wiki/GitLab_CE).


# 2. Pourquoi utiliser Framagit pour les projets de bac en ISN ?

Cela permet le suivi:
 * des difficultés rencontrées
 * des progrès d'une semaine sur l'autre
 * des aides fournies par les professeurs successifs (conseils, réorientation du
 projet par exemple).

De plus, cela fait un premier contact avec une forge logicielle. C'est utilisé dans certaines filières post bac, et aussi dans certains métiers de l'informatique.

# 3. Comment utiliser Framagit ?

L'organisation envisagée ici est une adaptation du fonctionnement testé en ISN
en 2015-2016 au lycée Malraux.

Nous utiliserons deux fonctionnalités de Framagit:
 * [x] Issues
 * [x] Repository

Pour que l'outil soit rapide à prendre en main, nous désactiverons :
 * [ ] Merge Requests
 * [ ] Pipelines
 * [ ] Wiki
 * [ ] Snippets

## 3.1 Les "Problèmes" (*Issues*)

Un problème (en anglais: "issue") désigne par exemple un problème rencontré, que l'on
souhaite résoudre. On commence par "ouvrir un problème", ce qui correspond à la
désignation d'un numéro de fil de discussion comme dans un forum, dans lequel on
décrit la situation. On le "ferme" quand il est résolu. Cela peut servir :
 * pour partager le travail à faire,
 * pour garder une trace du questionnement, des décisions prises, des obstacles
surmontés, des idées abandonnées, etc.
 * pour faciliter la communication entre les professeurs successifs (conseils
donnés, pistes à éviter, pistes à explorer)

Par exemple, dans l'image ci-dessous, voici des problèmes rangés en [tableau
kanban](https://fr.wikipedia.org/wiki/Tableau_kanban) suivant la situation de chacun (en attente, en cours,
réalisé):

![capture d'écran du tableau des issues](doc/board.png?raw=true "capture d'écran du tableau des issues")

## 3.2 Le dépot (*Repository*)

Cela sert de dépôt de fichiers.
 * On peut y inclure une documentation (avec le fichier README.MD)
 * Il contient un système de suivi de versions. On peut suivre l'évolution du
 projet dans le temps. On peut afficher les différences entre deux versions
 d'un fichier.
 * Chaque ajout ou modification de fichier s'appelle une validation (en anglais: "commit").
 * On peut commenter le code de chaque validation, comme le montre la capture
 d'écran ci-dessous. (La flèche 1 montre où cliquer pour faire un
nouveau commentaire, l'accolade 2 montre un commentaire déjà écrit, auquel il
est possible de répondre.)
![capture d'écran des commentaires](doc/commentaire.png?raw=true "capture d'écran des commentaires")
 * Pour télécharger un fichier (.py par exemple) qui est sur le dépôt: avec le 
navigateur, sélectionner le fichier dans le dépôt puis faire un clic droit sur
le bouton `open raw` et choisir `enregistrer la cible du lien sous...`

# 4. Organisation

## 4.1 du côté professeurs

* Un dépôt privé par projet dont les membres sont les élèves et les professeurs
(activer seulement Issues et Repository)
* Chaque dépôt fait partie du groupe framagit
@projets-bac-2018-isn-montesquieu-72.
* Toute évolution importante ou ajustement proposé par un professeur fait
l'objet d'un "problème" qui devra être ensuite géré par les élèves (discussion
éventuelle, traitement, clôture).

## 4.2 du côté élèves

* Un compte par élève.
* Toute question importante pour le suivi du projet doit être rédigée dans un
"problème". Ainsi, chaque professeur peut la consulter par la suite.
* Idéalement, **chaque séance** doit donner lieu à **au moins 2 validations** par
élève.  
Explication de cette règle : cela permet une utilisation suffisante de
la plateforme, et cela permet de comprendre qui avance comment, et sur quel
domaine. Par ailleurs, faire plusieurs validations, cela oblige à se
poser la question de ce qui doit être validé et d'avancer tâche par tâche (et
non pas par gros paquets de modifications dans tous les sens sans cohérence sur
la validation). C'est une façon de faire appelée parfois "commit early and
often".

# 5. Comment faire une validation dans le dépôt ?

## 5.1. Méthode conseillée (plus simple, mais plus limitée) avec l'interface web

**Dans le menu `Files`, on peut** : 
- Créer un nouveau fichier avec `Create file`
- Téléverser un nouveau fichier avec `Upload file`
- Créer un nouveau dossier avec `New directory`

![capture d'écran](doc/Capture-1.JPG?raw=true "capture d'écran")

**Dans le menu `Files`, après avoir cliqué sur un fichier particulier, on peut l'éditer (`EDIT`), le remplacer (`REPLACE`), ou le supprimer (`REMOVE`)**

![capture d'écran](doc/Capture-2.JPG?raw=true "capture d'écran")

**Dans l'exemple ci-dessous, on décide de mettre à jour le fichier `factorielle.py` en le remplaçant par sa nouvelle version.**

![capture d'écran](doc/Capture-3.JPG?raw=true "capture d'écran")

**L'image suivante est celle qui prépare le contenu de la validation :**

![capture d'écran](doc/Capture-4.JPG?raw=true "capture d'écran")

**Une fois que la validation est effectuée, elle est visible sous la forme suivante :**

![capture d'écran](doc/Capture-5.JPG?raw=true "capture d'écran")

## 5.2. Autre méthode (plus compliquée, mais beaucoup plus riche)

En fait il est plus efficace d'utiliser git en ligne de commande. Cela donne
davantage de possibilités dans les validations (en particulier quand on veut
modifier plusieurs fichiers dans la même validation).
C'est plutôt ainsi que les développeurs procèdent.

Voir [cette page](memento-git.md) pour davantage d'informations.
