# 1. Installation de git

Sous Debian : `sudo apt-get update` puis `sudo apt-get install git`.

Sous Windows (au lycée en particulier) on peut installer
**Git for Windows Portable ("thumbdrive edition")**.
https://git-scm.com/download/win

# 2. Documentation

[Git Tutorial](https://try.github.io/levels/1/challenges/1) pour apprendre git en ligne sur des exemples.

[Git Cheatsheet](http://ndpsoftware.com/git-cheatsheet/previous/git-cheatsheet.html) (disponible en français) concernant les commandes et les espaces:
- remise
- espace de travail
- index
- dépôt local
- dépôt distant.

[AIDE-MÉMOIRE GITHUB GIT](https://services.github.com/on-demand/downloads/fr/github-git-cheat-sheet.pdf)

[Livre de référence](https://git-scm.com/book/fr/v2) en français.

[Lien principal](https://git-scm.com/) contenant toute la documentation utile.

# 3. Mémento - git en ligne de commande

## 3.1 Accès au dépôt

1. Créer un dépot sur framagit, et récupérer son url (`url`).
2. Suivant la situation :
 - Cloner le dépôt existant : `git clone url`
 - `git fetch origin` pour tirer tout ce qui a été mis à jour dans le dépôt
 distant.
 - `git push origin master` pour pousser notre branche `master` vers le serveur
 `origin`

## 3.2 Pour chaque commit

1. `git status`
2. `git add monfichier.py`
3. `git status`
4. (facultatif) `git diff` pour comparer la répertoire de travail avec la zone d'index
5. (facultatif) `git diff --staged` pour visualiser les modifications indexées qui feront
 partie de la prochaine validation 
6. `git commit -m "message"`